FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > poppler.log'

COPY poppler .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' poppler
RUN bash ./docker.sh

RUN rm --force --recursive poppler
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD poppler
